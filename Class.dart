class awards: 
    def __init__(self, name, year, category): 
        self.name = name
        self.year = year
        self.category = category
      
list = [] 
  
# appending instances to list 
list.append(awards('FNB Banking App', 2012, 'Best Android Consumer App'))
list.append(awards('Nedbank App Suite', 2013, 'Best Android Consumer App'))
list.append(awards('My Belongings', 2014, 'Best Android Consumer App'))
list.append(awards('DStv Now', 2015, 'Best Android Consumer App'))
list.append(awards('Takealot', 2016, 'Best Android Consumer App'))
list.append(awards('Hey Jude', 2017, 'Best Android Consumer App'))
list.append(awards('Khula Ecosystem', 2018, 'Best Android Consumer App'))
list.append(awards('Naked', 2019, 'Best Android Consumer App'))
list.append(awards('EasyEquities', 2020, 'Best Android Consumer App'))
list.append(awards('Ambani Africa', 2021, 'Best Android Consumer App'))


for obj in list:
    print( obj.name, obj.year, obj.category)
